#
#  Copyright 2020 Jeremy Potter <jwinnie@stormdesign.us>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import os
import shutil
import tarfile
import mimetypes
from pathlib import Path

import tqdm
import requests

from config import config
from cli import log

#
# Download tarballs
#

def download():
    # Create directory 'output/sources' if it doesn't already exist
    Path("output/sources").mkdir(parents=True, exist_ok=True)
    for name, url in config.software_urls.items():
        filename = f"{name}-{config.software_versions[name]}.tar.{config.software_formats[name]}"
        log("download", filename)
        http_conn = requests.get(url, stream=True)
        total_size = int(http_conn.headers.get("content-length", 0))
        block_size = 1024
        with tqdm.tqdm(total=total_size, unit="iB", unit_scale=True) as progress_bar:
            with open(os.path.join("output", "sources", filename), "wb") as downloaded_file:
                for data in http_conn.iter_content(block_size):
                    progress_bar.update(len(data))
                    downloaded_file.write(data)

#
# Unpack tarballs
#

def track_progress(progress_bar, tar):
    for file in tar:
        progress_bar.set_description(file.name)
        progress_bar.update(1)
        yield file

def extract():
    # Create directory 'output/sources_unpacked' if it doesn't already exist
    Path("output/sources_unpacked").mkdir(parents=True, exist_ok=True)
    for filename in os.listdir(os.path.join("output", "sources")):
        log("extract", filename)
        with tarfile.open(os.path.join("output", "sources", filename)) as tar:
            with tqdm.tqdm(total=sum(1 for file in tar if file.isreg()), unit="files") as progress_bar:
                    tar.extractall("output/sources_unpacked", members=track_progress(progress_bar, tar))
    # Make a backup so if the build step fails you can start over without having to extract again
    log("backup", "Making backup of unpacked sources (this may take some time)...")
    shutil.copytree("output/sources_unpacked", "output/sources_unpacked_backup")
