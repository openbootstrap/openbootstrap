#
#  Copyright 2020 Jeremy Potter <jwinnie@stormdesign.us>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import os
import sys
import shutil

from config import config
import compilation
import tarballs
from cli import log, log_error, B, CLR

#
# In case the build fails and you need to reset OpenBootstrap to the state before the build.
#

def reset_build():
    log("delete", "Deleting build output (this may take some time)...")
    shutil.rmtree("output/toolchain")
    log("restore", "Removing temporary build files (this may take some time)...")
    shutil.rmtree("output/sources_unpacked")
    log("restore", "Restoring backup of unpacked sources (this may take some time)...")
    shutil.copytree("output/sources_unpacked_backup", "output/sources_unpacked")

#
# Clean up everything (output, temporary build files, downloaded source files, etc.)
#

def clean():
    if os.path.exists("output"):
        log("clean", "Clearing all build files and output (this may take some time)...")
        shutil.rmtree("output")
    else:
        log("skip", "Your OpenBootstrap is already clean")

#
# Command line interface (primitive argparse implementation)
#

HELP_MESSAGE = f"""{B}OpenBootstrap{CLR}
Copyright (c) 2020 Jeremy Potter

Usage: openbootstrap.py [command] [subcommand...]

{B}download{CLR} - download the source tarballs
{B}extract{CLR} - extract the source tarballs
{B}compile{CLR} - compile the toolchain
    You can specify which specific steps to compile using the subcommand, as in 'openbootstrap.py compile step1 step2 step3 ...'
{B}reset-build{CLR} - run this in case the compilation fails to restore clean source files
{B}all{CLR} - start from scratch and do a full build of the toolchain (download -> extract -> compile)
{B}clean{CLR} - clean up everything

It is recommended to run the steps in succession manually rather than using 'all' to make sure that each step ran properly before proceeding to the next."""

if __name__ == "__main__":
    try:
        command = sys.argv[1]
        if command == "download":
            tarballs.download()
        elif command == "extract":
            tarballs.extract()
        elif command == "compile":
            # Prepare the compilation environment
            env = compilation.env_prepare()
            # If a specific script is specified, run that script.
            # Otherwise, run the compilation scripts in succession
            try:
                scripts_specified = sys.argv[2:]
                for script_specified in scripts_specified:
                    compilation.compile_pkg(script_specified, config.scripts[script_specified], env)
            except IndexError:
                for script, pkg in config.scripts.items():
                    compilation.compile_pkg(script, pkg, env)
        elif command == "all":
            tarballs.download()
            tarballs.extract()
            # Prepare the compilation environment,
            # then run the compilation scripts in succession
            env = compilation.env_prepare()
            for script, pkg in config.scripts.items():
                compilation.compile_pkg(script, pkg, env)
        elif command == "reset-build":
            reset_build()
        elif command == "clean":
            clean()
        elif command == "help" or command == "--help":
            print(HELP_MESSAGE)
        else:
            log_error("Command not recognized. Use 'openbootstrap.py help' to get a list of possible commands.")
    except IndexError:
        # No command specified
        log_error("No command specified. Use 'openbootstrap.py help' to get a list of possible commands.")
